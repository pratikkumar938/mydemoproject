# mydemoproject

This is a demo project for deploying hello world flask app using gitlab ci/cd.  


## This project contains :
- base-infra

This contains the terraform code to launch a server with minimal configuration. It takes VPC ID, Region, Subnet ID as input varibale and launch a ubuuntu server which has 8080 and 22(just for the demo) port open for the world

- intial-deploy

This contains the configuration we need to do post launch. For the demo we are assuming that there is not org level private AMI available.

- api-setup

This contains the api code and a script to restart the api post deployment

## Change Flow
One we have a change, ci/cd jobs gets triggered syncing the latest to ther server followed up with the stop and start of flask api. 
For this demo, pre/post verification steps are excluded

## Sample Output

`pratik.kumar@Pratiks-MacBook-Pro hellow-world % curl http://18.219.120.64:8080/

{
  "Message": "Hello From Demo"
}

pratik.kumar@Pratiks-MacBook-Pro hellow-world % `
