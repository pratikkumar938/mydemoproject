#!/bin/bash
cd /home/gitlab-runner/api-setup
forever stop app.py
sudo kill $(ps aux | grep 'app.py' | awk '{print $2}')
forever start -c /usr/bin/python3 app.py
