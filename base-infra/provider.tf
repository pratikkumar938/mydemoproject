# Configure the AWS Provider
provider "aws" {
  version = "~> 3.0"
  shared_credentials_file = "/Users/pratik.kumar/.aws-my/creds"
  region  = "us-east-2"
}
