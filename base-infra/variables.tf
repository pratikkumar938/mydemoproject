variable "region" {
  type        = string
  description = "Region"
  default     = "us-east-2"
}

variable "vpc_id" {
  type        = string
  description = "VPC for the instance"
  default     = "vpc-2266e649"
}

variable "subnet_id" {
  type        = string
  description = "Name of the subnet"
  default     = "subnet-c4662f88"
}


