#!/bin/bash
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install nodejs
sudo npm install forever -g
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get install python3.6
sudo apt install python3-pip
pip3 install flask
